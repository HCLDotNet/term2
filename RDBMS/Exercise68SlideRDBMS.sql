-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE IncrementAge
	-- Add the parameters for the stored procedure here
	@specifiedId int,
	@age int OUT 
AS
BEGIN
	DECLARE @oldAge int
	DECLARE @studentAge int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @oldAge = age FROM Student WHERE sid = @specifiedId
	SELECT @studentAge = age FROM Student WHERE sid = 2
	SET @age = @oldAge + @studentAge
END
GO
