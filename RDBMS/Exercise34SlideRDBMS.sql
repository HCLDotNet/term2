SELECT s.SID,c.CName FROM Student s, Enrolment e, Course c WHERE s.SID = e.SID AND c.CID = e.CID AND c.CName='Java'
UNION ALL
SELECT s.SID,c.CName FROM Student s, Enrolment e, Course c WHERE s.SID = e.SID AND c.CID = e.CID AND c.CName='RDBMS'

SELECT s.SID FROM Student s, Enrolment e, Course c WHERE s.SID = e.SID AND c.CID = e.CID AND c.CName='Java'
INTERSECT
SELECT s.SID FROM Student s, Enrolment e, Course c WHERE s.SID = e.SID AND c.CID = e.CID AND c.CName='RDBMS'

SELECT s.SID FROM Student s, Enrolment e, Course c WHERE s.SID = e.SID AND c.CID = e.CID AND c.CName='Java'
EXCEPT
SELECT s.SID FROM Student s, Enrolment e, Course c WHERE s.SID = e.SID AND c.CID = e.CID AND c.CName='RDBMS'
