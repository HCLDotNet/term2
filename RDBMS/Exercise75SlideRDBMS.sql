CREATE FUNCTION calculateAge(@dob Date)
RETURNS INT
BEGIN
	RETURN YEAR(GETDATE()) - YEAR(@dob)
END

-- Remember to state dbo as prefix to work--
SELECT dbo.calculateAge('2007-12-12')


ALTER PROCEDURE insertStudent
	@name nvarchar(30),
	@dob date
AS 
BEGIN
	DECLARE @maxId int
	DECLARE @calculatedAge int
	
	SELECT @maxId = MAX(sid) FROM Student

	SELECT @calculatedAge = dbo.calculateAge(@dob)
	INSERT INTO Student(sid, sname,age) VALUES(@maxId + 1, @name, @calculatedAge)
END