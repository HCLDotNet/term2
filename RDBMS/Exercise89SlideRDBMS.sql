USE [RDBMS]
GO
/****** Object:  Trigger [dbo].[RejectInsertUpdate]    Script Date: 7/14/2021 10:44:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[RejectInsertUpdate] On [dbo].[Enrolment]
AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @enrolmentDate date
	DECLARE EnrolmentLoop CURSOR FOR SELECT edate FROM Enrolment
	OPEN EnrolmentLoop
	FETCH NEXT FROM EnrolmentLoop INTO @enrolmentDate

	WHILE(@@FETCH_STATUS = 0)
		BEGIN
			IF(DATEDIFF(DAY, @enrolmentDate, GETDATE()) > 5)
			BEGIN
				RAISERROR('Enrolment date is more than 5 days before current date', 1, 1)
				ROLLBACK TRANSACTION
			END
			CLOSE EnrolmentLoop
			FETCH NEXT FROM EnrolmentLoop
		END
	
ROLLBACK TRANSACTION
END