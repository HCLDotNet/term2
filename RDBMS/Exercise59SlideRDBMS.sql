CREATE VIEW employee_with_course
AS
SELECT Student.*, Course.*,Enrolment.edate FROM Student INNER JOIN Enrolment on Student.sid = Enrolment.sid
INNER JOIN Course ON Enrolment.cid = Course.cid

SELECT * FROM employee_with_course WHERE MONTH(edate) = 7