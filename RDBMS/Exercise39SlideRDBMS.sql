SELECT * FROM student WHERE age <= ALL (SELECT age from student where sname LIKE '%y')

SELECT * FROM student s WHERE EXISTS (SELECT * FROM enrolment e, course c WHERE s.sid = e.sid AND e.cid = c.cid AND c.cname = 'Java')

SELECT * FROM student WHERE sid NOT IN (2,4)

SELECT sid, sname FROM student s WHERE EXISTS (SELECT * FROM enrolment e WHERE edate ='2006-09-01' AND e.sid = s.sid)

SELECT * FROM course WHERE duration >= ALL (SELECT duration from course)

SELECT * FROM student s WHERE age <= ALL(SELECT age FROM student) AND EXISTS (SELECT * FROM enrolment e, course c WHERE s.sid = e.sid AND e.cid = c.cid AND c.cname LIKE '%Java%')
