create table Student (
	sid int primary key,
	sname nvarchar(100),
	age int,

	constraint chk_age CHECK (age between 20 and 40)
)

create table Course (
	cid int primary key,
	cname nvarchar(100),
	duration int

)

create table Enrolment (
	sid int,
	cid int,
	edate date

	foreign key (sid) references Student(sid),
	foreign key (cid) references Course(cid)
)
