﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ADONETWebFormDemo
{
    public partial class GridView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Find LinkButton where id = LinkButton1
                LinkButton addButon = (LinkButton)e.Row.FindControl("LinkButton1");
                //set command argument for linkbutton to retrieve later = row index
                addButon.CommandArgument = e.Row.RowIndex.ToString();
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Command name is declared in View of .apsx
            if (e.CommandName == "Add")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow gridViewRow = GridView1.Rows[index];
                ListItem listItem = new ListItem();
                listItem.Text = gridViewRow.Cells[2].Text;
                //If item is not exist in ListBox, then add to ListBox
                if (!ListBox1.Items.Contains(listItem))
                {
                    ListBox1.Items.Add(listItem);
                }
            }
        }
    }
}