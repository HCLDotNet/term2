﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ADONETWebFormDemo
{
    public partial class XML : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private void BindGridView()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("SELECT * FROM CD", conn);
            
            DataSet dataSet = new DataSet();
            SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
            
            dataSet.ReadXml(@"C:\Users\Administrator\Desktop\HCL\Term2\ADONET\ADONETWebFormDemo\cd_catalog.xml");
            GridView1.DataSource = dataSet;
            GridView1.DataBind();

            //Update source table
            sqlDataAdapter.Update(dataSet, "CD");

            conn.Close();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BindGridView();            
        }
    }
}