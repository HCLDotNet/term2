﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADONET
{
    class SQLDataBuilder
    {
        public static void Run()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM employee", sqlConnection);
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            //Data is pushed into dataset to use offline (disconnected architecture)
            DataSet dataSet = new DataSet();
            sqlDataAdapter.Fill(dataSet);

            //Using sqlcommand builder to update data from dataset to datasource.
            SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);
            sqlCommandBuilder.GetUpdateCommand();

            //Get first record for update
            DataTable employees = dataSet.Tables[0];
            employees.Rows[0]["name"] = "updated camnh";
            //Update dataset source table
            sqlDataAdapter.Update(dataSet);

            foreach (DataRow dataRow in dataSet.Tables[0].Rows)
            {
                Console.WriteLine("ID: {0} - Name: {1} - DeptCode: {2}", dataRow[0], dataRow[1], dataRow[2]);
            }

            sqlCommandBuilder.GetInsertCommand();
            //For exercise slide 64
            DataRow newRow = employees.NewRow();
            //getting the first column and populate data
            newRow["ID"] = 11;
            newRow["name"] = "new camnh";
            newRow["dept"] = "ITA";
            //add new row to datatable
            employees.Rows.Add(newRow);
            sqlDataAdapter.Update(dataSet);
        }
    }
}
