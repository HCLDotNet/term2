﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADONET
{
    class StoredProcedure
    {
        public static void Run()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = "EmpInsertWithoutOutput";
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@empName", "tranglt");
            sqlCommand.Parameters.AddWithValue("@dept", "NZ");            
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public static void ReturnOuput()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = "EmpInsert";
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@empName", "abc");
            sqlCommand.Parameters.AddWithValue("@dept", "NZ");
            //return something from stored proc.
            SqlParameter sqlParameter = new SqlParameter("@maxId", System.Data.SqlDbType.Int);
            sqlParameter.Direction = System.Data.ParameterDirection.ReturnValue;
            sqlCommand.Parameters.Add(sqlParameter);
            sqlCommand.ExecuteNonQuery();

            Console.WriteLine("Added Id in stored procedure {0}", sqlCommand.Parameters["@maxId"].Value.ToString());
            sqlConnection.Close();
        }
    }
}
