﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADONET
{
    class BasicSqlDataReader
    {
        public static void Run()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";
            
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM employee; SELECT * FROM Dept", sqlConnection);
            SqlDataReader reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine("ID:" + reader.GetInt32(0));
                Console.WriteLine("Name:" + reader.GetString(1));
                Console.WriteLine("Dept:" + reader.GetString(2));
            }

            reader.NextResult();
            while (reader.Read())
            {
                Console.WriteLine("ID:" + reader.GetString(0));
                Console.WriteLine("Name:" + reader.GetString(1));
            }
            sqlConnection.Close();
        }
    }
}
