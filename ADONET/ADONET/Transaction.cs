﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADONET
{
    class Transaction
    {
        public static Dictionary<string,string> Setup()
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            Console.WriteLine("Enter employee name");
            string empName = Console.ReadLine();
            Console.WriteLine("Enter dept description");
            string deptDescription = Console.ReadLine();
            Console.WriteLine("Enter dept code");
            string deptCode = Console.ReadLine();

            keyValuePairs.Add("empName",empName);
            keyValuePairs.Add("deptDescription", deptDescription);
            keyValuePairs.Add("deptCode", deptCode);

            return keyValuePairs;
        }

        public static void Run()
        {
            Dictionary<string, string> keyValuePairs = Setup();
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";

            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();

            SqlTransaction sqlTransaction = sqlConnection.BeginTransaction();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            sqlCommand.Transaction = sqlTransaction;
            //transaction surround with try catch
            SqlDataReader sqlDataReader = null;
            try
            {
                sqlCommand.CommandText = "SELECT * FROM Dept where DeptId = '" + keyValuePairs["deptCode"] + "'";
                sqlDataReader = sqlCommand.ExecuteReader();                
                sqlDataReader.Read();

                if (!sqlDataReader.HasRows)
                {
                    sqlDataReader.Close();
                    sqlCommand.CommandText = "INSERT INTO Dept VALUES('" + keyValuePairs["deptCode"] + "','" + keyValuePairs["deptDescription"] + "')";
                    sqlCommand.ExecuteNonQuery();
                }
                              

                sqlCommand.CommandText = "SELECT MAX(Id) FROM Employee";
                sqlDataReader = sqlCommand.ExecuteReader();
                sqlDataReader.Read();
                int id = sqlDataReader.GetInt32(0);
                sqlDataReader.Close();
                sqlCommand.CommandText = "INSERT INTO Employee VALUES ("+(id + 1)+", '"+keyValuePairs["empName"]+"', 'asdasdasdadasdasdadsasads')";
                //close reader before executeNonQuery()
                sqlCommand.ExecuteNonQuery();
                sqlDataReader.Close();
                //call commit to apply to database
                sqlTransaction.Commit();
                Console.WriteLine("Inserted successfully");
                
            } catch (Exception e)
            {
                sqlDataReader.Close();
                Console.WriteLine("Transaction failed: " + e.Message);
                Console.WriteLine("Stack trace: " + e.StackTrace);
                //if in any situation, rollback things.
                sqlTransaction.Rollback();
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }
}
