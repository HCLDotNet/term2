﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADONET
{
    class Program
    {
        static void Main(string[] args)
        {
            //BasicSqlDataReader.Run();

            Transaction.Run();

            //StoredProcedure.Run();

            //StoredProcedure.ReturnOuput();

            //DataSetLesson.Run();

            //SQLDataBuilder.Run();

            Console.ReadLine();
        }
    }
}
