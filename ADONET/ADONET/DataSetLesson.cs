﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADONET
{
    class DataSetLesson
    {
        public static void Run()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("SELECT * FROM employee", sqlConnection);
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            //Data is pushed into dataset to use offline (disconnected architecture)
            DataSet dataSet = new DataSet();
            sqlDataAdapter.Fill(dataSet);

            DataTable dataTable = dataSet.Tables[0];

            foreach (DataRow dataRow in dataTable.Rows)
            {
                Console.WriteLine("ID: {0} - Name: {1} - DeptCode: {2}", dataRow[0], dataRow[1], dataRow[2]);
            }
        }
    }
}
