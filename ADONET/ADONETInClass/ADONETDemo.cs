﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADONETInClass
{
    class ADONETDemo
    {
        public static void Run()
        {
            Console.WriteLine("Enter ID");
            int id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter dept");
            string dept = Console.ReadLine();

            //1st step connection
            //2nd get connection - connectionString
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            //logic goes here
            SqlCommand command = new SqlCommand("INSERT INTO Employee(ID,name,dept) VALUES (@Id,@Name,@Dept)", conn);
            //SqlParameter idParameter = new SqlParameter();
            //idParameter.ParameterName = "@Id";
            //idParameter.Value = id;
            //command.Parameters.Add(idParameter);
            command.Parameters.AddWithValue("@Id", id);
            command.Parameters.AddWithValue("@Name", name);
            command.Parameters.AddWithValue("@Dept", dept);
            //execute command

            //perform update and delete within employee table using SQLCommand
            //Console: 1. Update 2. Delete
            //Console: Enter ID to update:
            //Console: Enter name to update
            //Console: Enter dept to update

            //Console: Enter ID for delete
            //==>SqlCOmmand to delete record from database where employee = ID.
            command.ExecuteNonQuery();
            conn.Close();
        }

        public static void RunSelect()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlCommand command = new SqlCommand("SELECT * FROM Employee;SELECT * FROM Dept", conn);
            #region First query
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Console.WriteLine(reader.GetInt32(0));//ID
                    Console.WriteLine(reader.GetString(1)); //Name
                    Console.WriteLine(reader.GetString(2));//Dept
                    Console.WriteLine("------");
                }
            }
            else {
                Console.WriteLine("No row returned");
            }
            #endregion
            #region Second query
            reader.NextResult();//Switch from the first statement select to 2nd statement select.
            #endregion
            Console.WriteLine("========");
            while (reader.Read())
            {
                Console.WriteLine(reader.GetString(0));//ID
                Console.WriteLine(reader.GetString(1)); //Name
                Console.WriteLine("------");
            }
            reader.Close();

            conn.Close();

        }

        public static void RunTransaction()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlTransaction transaction = conn.BeginTransaction();
            try
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.CommandText = "INSERT INTO Dept(deptID, name) VALUES(11, 'abcdef') ";
                sqlCommand.Connection = conn;
                sqlCommand.Transaction = transaction;
                sqlCommand.ExecuteNonQuery();

                sqlCommand.CommandText = "INSERT INTO Employee(ID, name, dept) VALUES (15, 'thangnx', '2345asdasdas6')";
                sqlCommand.ExecuteNonQuery();
                transaction.Commit();
                Console.WriteLine("Success");
            }
            catch (Exception e) {
                Console.WriteLine("Message: {0}", e.Message);
                Console.WriteLine("Stack trace: {0}", e.StackTrace);
                transaction.Rollback();
            }
            //SELECT
            //INSERT INTO Dept
            //SELECT
            //INSERT INTO Employee
            conn.Close();
        }
    }
}
