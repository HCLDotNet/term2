﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADONETInClass
{
    class ADOStoredProcedure
    {
        public static void Run()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "SearchByEmployeeName"; //stored procedure name
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure; //this specifies Store Procedure command type
            sqlCommand.Connection = conn;

           

            sqlCommand.Parameters.Add("@name", SqlDbType.Char, 50).Value = "thangnx"; //attach parameter to sqlCommand

            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                Console.WriteLine(sqlDataReader.GetInt32(0));
                Console.WriteLine(sqlDataReader.GetString(1));
                Console.WriteLine(sqlDataReader.GetString(2));
            }

            sqlDataReader.Close();
            conn.Close();
        }

        public static void RunProcedureOut()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = conn;
            sqlCommand.CommandText = "SearchByDeptID";
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@DeptID", SqlDbType.Char).Value = "HRI";
            sqlCommand.Parameters.Add("@Name",SqlDbType.Char,50).Direction = ParameterDirection.Output;

            sqlCommand.ExecuteNonQuery();

            Console.WriteLine(sqlCommand.Parameters["@Name"].Value);//This is how out value in stored return
            conn.Close();
        }

        public static void RunProcedureReturn()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=EmployeeMgmt;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = conn;
            sqlCommand.CommandText = "SearchByDeptIDReturn";
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            sqlCommand.Parameters.Add("@deptID", SqlDbType.Char, 10).Value = "HRI";
            sqlCommand.Parameters.Add("@numCount", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

            sqlCommand.ExecuteNonQuery();

            Console.WriteLine(sqlCommand.Parameters["@numCount"].Value.ToString());//This is how out value in stored return
            conn.Close();
        }
    }
}