USE [master]
GO
/****** Object:  Database [StudentEnrolment]    Script Date: 09/08/2021 08:48:02 ******/
CREATE DATABASE [StudentEnrolment] ON  PRIMARY 
( NAME = N'StudentEnrolment', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\StudentEnrolment.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'StudentEnrolment_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\StudentEnrolment_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [StudentEnrolment] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [StudentEnrolment].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [StudentEnrolment] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [StudentEnrolment] SET ANSI_NULLS OFF
GO
ALTER DATABASE [StudentEnrolment] SET ANSI_PADDING OFF
GO
ALTER DATABASE [StudentEnrolment] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [StudentEnrolment] SET ARITHABORT OFF
GO
ALTER DATABASE [StudentEnrolment] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [StudentEnrolment] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [StudentEnrolment] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [StudentEnrolment] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [StudentEnrolment] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [StudentEnrolment] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [StudentEnrolment] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [StudentEnrolment] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [StudentEnrolment] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [StudentEnrolment] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [StudentEnrolment] SET  DISABLE_BROKER
GO
ALTER DATABASE [StudentEnrolment] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [StudentEnrolment] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [StudentEnrolment] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [StudentEnrolment] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [StudentEnrolment] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [StudentEnrolment] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [StudentEnrolment] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [StudentEnrolment] SET  READ_WRITE
GO
ALTER DATABASE [StudentEnrolment] SET RECOVERY SIMPLE
GO
ALTER DATABASE [StudentEnrolment] SET  MULTI_USER
GO
ALTER DATABASE [StudentEnrolment] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [StudentEnrolment] SET DB_CHAINING OFF
GO
USE [StudentEnrolment]
GO
/****** Object:  Table [dbo].[Enrolment]    Script Date: 09/08/2021 08:48:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enrolment](
	[SID] [int] NOT NULL,
	[CID] [int] NOT NULL,
	[EDate] [date] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Folder]    Script Date: 09/08/2021 08:48:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folder](
	[FolderID] [int] NOT NULL,
	[FolderName] [nvarchar](50) NOT NULL,
	[ParentID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 09/08/2021 08:48:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[ID] [int] NULL,
	[SName] [nvarchar](50) NULL,
	[Age] [int] NULL,
	[Message] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 09/08/2021 08:48:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[SID] [int] NOT NULL,
	[SName] [nvarchar](50) NOT NULL,
	[Age] [tinyint] NULL,
	[CreditCardNumber] [nvarchar](50) NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[SID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 09/08/2021 08:48:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[CID] [int] NOT NULL,
	[CName] [nvarchar](50) NOT NULL,
	[Duration] [int] NOT NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[CID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[StudentView]    Script Date: 09/08/2021 08:48:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[StudentView]
AS
SELECT SID, SName FROM Student
GO
/****** Object:  UserDefinedFunction [dbo].[calculateStudentNameLength]    Script Date: 09/08/2021 08:48:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[calculateStudentNameLength](@SName nvarchar(50))
RETURNS int
AS
BEGIN
	DECLARE @tmp nvarchar(50)
	SELECT @tmp = SName FROM Student WHERE SName = @SName
	RETURN LEN(@tmp)
END
GO
/****** Object:  StoredProcedure [dbo].[incrAge]    Script Date: 09/08/2021 08:48:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[incrAge]
	@SName nvarchar(50),
	@incrementValue int,
	@outputAge int out
AS
BEGIN
	DECLARE @tmpAge int
	SELECT @tmpAge = Age FROM Student WHERE SName = @SName
	/**
	* Neu Age <= 21 thi cong them increment Value + 5 tuoi, neu age > 21 thi cong them increment value + 3 tuoi
	*/
	IF (@tmpAge <= 21)
		BEGIN 
			SET @outputAge = @tmpAge + @incrementValue + 5
		END
	ELSE
		BEGIN 
			SET @outputAge = @tmpAge + @incrementValue + 3
		END
END
GO
/****** Object:  StoredProcedure [dbo].[FindStudentBySNameHasOutCalculated]    Script Date: 09/08/2021 08:48:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FindStudentBySNameHasOutCalculated]
	@SName nvarchar(50),
	@SID int out
AS
BEGIN
	DECLARE @tmp int
	SET @tmp = (SELECT SID FROM Student WHERE SName = @SName)
	SET @SID = @tmp + 1
END
GO
/****** Object:  StoredProcedure [dbo].[FindStudentBySNameHasOut]    Script Date: 09/08/2021 08:48:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FindStudentBySNameHasOut]
	@SName nvarchar(50),
	@SID int out
AS
BEGIN
	SELECT @SID = SID FROM Student WHERE SName = @SName
END
GO
/****** Object:  StoredProcedure [dbo].[FindStudentBySName]    Script Date: 09/08/2021 08:48:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FindStudentBySName]
	@SName nvarchar(50)
AS
BEGIN
	SELECT * FROM Student WHERE SName = @SName
END
GO
/****** Object:  View [dbo].[FilterBill]    Script Date: 09/08/2021 08:48:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[FilterBill]
AS
SELECT * FROM Student WHERE SName LIKE '%Bill%' 
WITH CHECK OPTION
GO
/****** Object:  UserDefinedFunction [dbo].[extractStudentData]    Script Date: 09/08/2021 08:48:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[extractStudentData](@SName nvarchar(50))
RETURNS TABLE 
AS
RETURN SELECT * FROM Student WHERE SName = @SName
GO
